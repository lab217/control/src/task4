from src import archive
import matplotlib.pyplot as plt
import xlrd

file = xlrd.open_workbook('data.xls', formatting_info=True) #Чтение данных с Excel файла
sheet = file.sheet_by_index(0)
data = sheet.col_values(3, start_rowx=1, end_rowx=10002) #Выбор варианта, строки отсчёта и конца
data = [float(elem) for elem in data] #Беребор данных
data2 = data #Просто для уверенности, т.к. данные меняются, создаю новые массивы. Возможно не надо
data3 = data
data4 = data
data5 = data
data6 = data


#Данные без фильтрации.
fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
#Графики делаются отдельно - слишком много данных, большой график на все - затруднительно, не оптимально
ax1.plot(range(len(data)), data, 'k', label = 'без фильтра')
ax1.set_ylim([-17, 17])
ax1.grid()
ax1.legend()

#разграничитель графиков
fig.tight_layout()

#Экспоненциальная средняя
ax2.plot(range(len(data2)), archive.sma_filter(data2, 10), 'r', label = 'sma')
ax2.set_ylim([-17, 17])
ax2.grid()
ax2.legend()


fig.tight_layout()

#Медиана
ax3.plot(range(len(data3)), archive.smm_filter(data3, 10), 'b', label = 'smm')
ax3.set_ylim([-17, 17])
ax3.legend()
ax3.grid()

plt.show()


plt.plot(range(len(data4)), data4, 'k', label = 'без фильтра')
plt.plot(range(len(data5)), archive.sma_filter(data5, 10), 'r', label = 'sma')
plt.plot(range(len(data6)), archive.smm_filter(data6, 10), 'b', label = 'smm')
plt.ylim([-17, 17])
plt.legend()
plt.grid()
plt.show()