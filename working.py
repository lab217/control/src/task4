from src import archive
import matplotlib.pyplot as plt
import xlrd

file = xlrd.open_workbook('data.xls', formatting_info=True)
sheet = file.sheet_by_index(0)
data = sheet.col_values(3, start_rowx=1, end_rowx=10002)
data = [float(elem) for elem in data]
data2 = data
data3 = data


sma_result = archive.sma_filter(data, 3)
smm_result = archive.smm_filter(data, 3)
ema_result = archive.ema_filter(data, 3)

fig = plt.figure()
# plt.subplots_adjust(top=0.936, bottom=0.07, wspace=0.121, right=0.971, left=0.052)
fig.set_size_inches(35, 9)

fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
fig.suptitle('"task4"')


ax1.plot(data, 'k')
ax1.set_xlabel('Средняя простая скользящая')
ax1.plot(sma_result, 'y')
ax1.set_ylim([-17, 17])

fig.tight_layout()

# ax2.title('Средняя экспоненциальная скользящая')
ax2.plot(data2, 'k')
ax2.set_xlabel('Средняя экспоненциальная скользящая')
ax2.plot(ema_result, 'b')

fig.tight_layout()

# ax3.title('Медианный фильтр')
ax3.plot(data3, 'k')
ax3.plot(smm_result, 'r')

plt.show()