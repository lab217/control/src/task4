import xlrd
import matplotlib.pyplot as plt


wb=xlrd.open_workbook('data.xls')
mass = []
sheet = wb.sheet_by_index(0)
sheet.cell_value(1, 17)
file = xlrd.open_workbook('data.xls', formatting_info=True)
sheet = file.sheet_by_index(0)
data = sheet.col_values(3, start_rowx=1, end_rowx=10002)
data = [float(elem) for elem in data]
print(data)
def cm_to_inch(value):
    return value/2.54
plt.plot([ i for i in range(len(data))], data)
plt.xlim(0,11000)
plt.ylim(-25,25)
plt.show()